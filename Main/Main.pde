Flock flock;
Spaceship ship;

boolean destroy=false;
boolean lose=false;
Boid tmp;
Laser tmp2;


void setup() {
  lose=false;
  
  size(1000, 720);
  flock = new Flock();
  for (int i = 0; i < 50; i++) {
    flock.addBoid(new Boid(width/6,height/6));
  }
  ship=new Spaceship();
}
int i =0;
void draw() {
  if(lose==false)
  {
    background(50);
    flock.run();
    textSize(32);
    text("/50", 45, 30); 
    fill(0, 102, 153);
    
    textSize(32);
    text(i, 10, 30); 
    fill(0, 102, 153);
    lasercolide();
    shipcolide();
    ship.display();
    ship.update();
    
    if(i==50)
    {
      textSize(50);
    text("YOU WON", width/3.2, height/2); 
    fill(0, 102, 153);
    }
  }
  if(lose==true)
  {
    background(50);
    flock.run();
    
    textSize(50);
    text("Destroyed", width/3.2, height/2); 
    fill(255, 0, 153);
  }
}


void keyPressed()
{
  if (key=='w')
  {
    ship.thrust();
  }
  if(key=='a')
  {
    ship.turn(-ship.turning);
  }
  if(key=='d')
  {
    ship.turn(ship.turning);
  }
  if(key==' ')
  {
    ship.shoot();
  }
  if(key=='r')
  {
  setup();
  }
}

void lasercolide()
{
  if(ship.max_laser>0)
  {
    for (Boid b : flock.boids) 
    {
      for (Laser j : ship.lasers) 
      {
          destroy=j.checkCollision(b.position);
          if(destroy==true)
          {
            tmp=b;
          tmp2=j;
          break;
          }
      }
      if(destroy==true)
      break;
    }
    
    if(destroy==true)
    {
      ship.max_laser--;
    flock.boids.remove(tmp);
    ship.lasers.remove(tmp2);
    destroy=false;
    i++;
    }
  }
}


void shipcolide()
{
    for (Boid b : flock.boids) 
    {      
          lose=ship.checkCollision(b.position);
          if(lose==true)
          break;
    }    
}
