class Spaceship { 
  PVector position;
  PVector velocity;
  PVector acceleration;
  boolean del=false;
   ArrayList<Laser> lasers;
  Laser l;
  int max_laser;
  float topspeed = 4;

  float heading = 0;

  float r = 16;
  int currenttime = millis();
  int wait=110;
  int lasttime=millis();
  float turning=0.1;

  boolean thrusting = false;

  Spaceship() {
    position = new PVector(width/2,height/2);
    velocity = new PVector();
    acceleration = new PVector();
   lasers= new ArrayList<Laser>();
   
  } 

  void update() { 
    velocity.add(acceleration);
    velocity.limit(topspeed);
    position.add(velocity);
    acceleration.mult(0);
    borders();
    
  }

  void applyForce(PVector force) {
    PVector f = force.get();
    acceleration.add(f);
  }

  // Turn changes angle
  void turn(float a) {
    heading += a;
  }
  
  void thrust() {
    float angle = heading - PI/2;
    PVector force = PVector.fromAngle(angle);
    force.mult(1);
    applyForce(force); 
    
    force.mult(-2);
    thrusting = true;
  }

  void wrapEdges() {
    float buffer = r*2;
    if (position.x > width +  buffer) position.x = -buffer;
    else if (position.x <    -buffer) position.x = width+buffer;
    if (position.y > height + buffer) position.y = -buffer;
    else if (position.y <    -buffer) position.y = height+buffer;
  }
  
  void borders() {
    if (position.x < -r) position.x = width+r;
    if (position.y < -r) position.y = height+r;
    if (position.x > width+r) position.x = -r;
    if (position.y > height+r) position.y = -r;
  }



void shoot()
{
  currenttime = millis();
  if(currenttime-lasttime>=wait)
  {
    lasttime=currenttime;
    if(max_laser<5)
    {
       l = new Laser(heading,position);
       lasers.add(l);
       max_laser++;
    }
  }
}

void laserthing()
{
  if(max_laser>0)
  {
    for (Laser j : lasers) 
    {
        j.update(); 
        if((j.position.x<2)||(j.position.x>width-2)||(j.position.y<2)||(j.position.y>height+2))
        {
           l=j;
           del=true;
        }
    }    
  }
    
  if(del==true)
  {
     max_laser--;
    lasers.remove(l);
    del=false;
  }
}


  void display() { 
    stroke(0);
    strokeWeight(2);
    pushMatrix();
    translate(position.x,position.y+r);
    rotate(heading);
    fill(0,255,255);
    beginShape();
    vertex(-r,r);
    vertex(0,-r);
    vertex(r,r);
    endShape(CLOSE);
    rectMode(CENTER);
    popMatrix();
    
    thrusting = false;
    laserthing();
  }
  
  boolean checkCollision( PVector location)
  {
    if((position.x <= location.x+15)&&(position.x >= location.x-15)&&(position.y <= location.y+15)&&(position.y >= location.y-15))
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
